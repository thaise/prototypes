# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/bionic64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 3306, host: 3307

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

   #Provider-specific configuration so you can fine-tune various
   #backing providers for Vagrant. These expose provider-specific options.
   #Example for VirtualBox:

   config.vm.provider "virtualbox" do |vb|
     # Display the VirtualBox GUI when booting the machine
     vb.gui = false

     # Customize the amount of memory on the VM:
     vb.memory = "2048"
   end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
   echo "================================================================================"
   echo "Updating and Installing Required Packages"
   echo "================================================================================"
   cd /vagrant
   apt-get update -y
   apt-get install -y php nginx git vim composer \
   mysql-server-5.7 php-mysql \
   php7.2-intl php7.2-mbstring php7.2-json \
   php7.2-xml php7.2-curl php7.2-zip php7.2-mysql php7.2-fpm

   echo "================================================================================"
   echo "Installing CodeIgniter 4"
   echo "================================================================================"
   cd prototypes
   composer require codeigniter4/framework
   cp -a vendor/codeigniter4/framework/writable .
   cp -a vendor/codeigniter4/framework/phpunit.xml.dist .
   cp -a vendor/codeigniter4/framework/spark .
   cd ../

   echo "================================================================================"
   echo "Configuring Nginx"
   echo "================================================================================"
   sudo cp /vagrant/codeigniter  /etc/nginx/sites-available/
   sudo ln -s /etc/nginx/sites-available/codeigniter /etc/nginx/sites-enabled/
   sudo rm -f /etc/nginx/sites-enabled/default

   echo "================================================================================"
   echo "Configuring PHP-FPM"
   echo "================================================================================"
   sudo cp /vagrant/prototypes.conf /etc/php/7.2/fpm/pool.d/
   sudo rm -f /etc/php/7.2/fpm/pool.d/www.conf
   sudo systemctl enable php7.2-fpm
   sudo systemctl restart php7.2-fpm

   sudo systemctl restart nginx

   echo "================================================================================"
   echo "Configuring Database"
   echo "================================================================================"
   sudo cp /vagrant/mysqld.cnf  /etc/mysql/mysql.conf.d/
   sudo systemctl restart mysql
   sudo mysql -u root < /vagrant/database.sql

   SHELL
end
